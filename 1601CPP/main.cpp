#include <iostream>
using namespace std;

char toup(char p)
{
    if (p>90) return p-'a'+'A';
    else return p;
}

char todown(char p)
{
    if (p<91) return p-'A'+'a';
    else return p;
}

int main()
{
#ifndef ONLINE_JUDGE
   freopen("input.txt", "rt", stdin);
   freopen("output.txt", "wt", stdout);
#endif
    char c[10001];
    int n=0;
    while (std::cin.get(c[n])) ++n;
    bool up = true;
    for(int i=0; i<n; ++i)
    {
        switch (c[i])
        {
        case ',':
        case '-':
        case ':':
        case '\n':
        case '\r':
        case '"':
        case ' ': cout<<c[i]; break;
        case '!':
        case '?':
        case '.': up = true; cout<<c[i]; break;
        default: if (up) { c[i] = toup(c[i]); up = false; } else { c[i] = todown(c[i]); } cout<<c[i]; break;
        }
    }
    return 0;
}

