#include <iostream>

using namespace std;

int main()
{
    int frr[6] = {10, 50, 100, 500, 1000, 5000}, s=0, p=0, k;
    for(int i=0; i<6; ++i)
    {
        int c;
        cin >> c;
        if ((!p)&&(c)) p = frr[i];
        s += frr[i]*c;
    }
    cin>>k;
    p = (s-p)/k;
    s /= k;
    cout << s-p <<"\n";
    for(int i=p+1; i<=s; ++i) cout << i << " ";
   return 0;
}
